#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
002_display_fps.py

Open a Pygame window and display framerate.
Program terminates by pressing the ESCAPE-Key.

works with python2.7 and python3.4

URL    : http://thepythongamebook.com/en:part2:pygame:step002
Author : horst.jens@spielend-programmieren.at
License: GPL, see http://www.gnu.org/licenses/gpl.html
"""

#the next line is only needed for python2.x and not necessary for python3.x
from __future__ import print_function, division
import pygame
# Oh how I wish this could be done in irl
import math
import random

# Initialize Pygame.
pygame.init()
# Set size of pygame window.
screen=pygame.display.set_mode((640,480))
# Create empty pygame surface.
background = pygame.Surface(screen.get_size())
# Fill the background black color.
background.fill((0, 0, 0))
# Convert Surface object to make blitting faster.
background = background.convert()
# Copy background to screen (position (0, 0) is upper left corner).
screen.blit(background, (0,0))
# Create Pygame clock object.
clock = pygame.time.Clock()

mainloop = True
# Desired framerate in frames per second. Try out other values.
FPS = 60
# How many seconds the "game" is played.
playtime = 0.0


def ungulate(n):
    return n*n

while mainloop:
    # Do not go faster than this framerate.
    milliseconds = clock.tick(FPS)
    playtime += milliseconds / 1000.0
    print(playtime)
    middle_y = 240
    middle_x = 320

    wiggle = random.random()/10
    scale_y = 200
    for x in range(640):
        pygame.draw.line(background, (0, 255, 255), (x ,middle_y + scale_y*  math.sin(x +1 )),(x+1,middle_y + scale_y*  math.sin(x +1 )))
    screen.blit(background, (0,0))
    background.fill((0, 0, 0))
    for event in pygame.event.get():
        # User presses QUIT-button.
        if event.type == pygame.QUIT:
            mainloop = False
        elif event.type == pygame.KEYDOWN:
            # User presses ESCAPE-Key
            if event.key == pygame.K_ESCAPE:
                mainloop = False

    # Print framerate and playtime in titlebar.
    text = "FPS: {0:.2f}   Playtime: {1:.2f}".format(clock.get_fps(), playtime)
    pygame.display.set_caption(text)

    #Update Pygame display.
    pygame.display.flip()

# Finish Pygame.
pygame.quit()

# At the very last:
print("This game was played for {0:.2f} seconds".format(playtime))
